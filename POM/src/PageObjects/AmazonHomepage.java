package PageObjects;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import Utility.Baseclass;

public class AmazonHomepage extends Baseclass {
	
	Logger log = LogManager.getLogger(AmazonHomepage.class);
	
	public AmazonHomepage(WebDriver driver) {
		super(driver);
	}
	
	@FindBy(xpath = "//a[@title='Log in to your customer account']")
	public static WebElement Signin;
	
	@FindBy(xpath = "//input[@placeholder='Search']")
	public static WebElement SearchBox;
	
	@FindBy(xpath = "//button[@type='submit']")
	public static WebElement SearchButton;
	
	public void click_sigin() {
		
		log.info("Clicking signin button");
		System.out.println("Clicking signin button");
		clickelement(Signin);
		
		log.info("Entering the search term");
		System.out.println("Entering the search term");
		clickelement(SearchBox);
		
		log.info("Clicking Search Button");
		System.out.println("Clicking Search Button");
		clickelement(SearchButton);
	}

}
