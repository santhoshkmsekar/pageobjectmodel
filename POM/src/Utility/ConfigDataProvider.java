package Utility;

import java.util.Properties;

public class ConfigDataProvider {
	Properties pro;

	public ConfigDataProvider() {
		pro = new Properties();
		try {
			pro.load(getClass().getResourceAsStream("./Config.properties"));
		}

		catch (Exception e) {
			System.out.println("Not able to load config file>>" + e.getMessage());
		}
	}
	
	public String getDataFromConfig(String keyToSearch) {

		return pro.getProperty(keyToSearch);

	}

	public String getbrowser_name() {
		return pro.getProperty("BROWSER_NAME");
	}

	public String getapp_url() {
		return pro.getProperty("APP_URL");
	}
}
