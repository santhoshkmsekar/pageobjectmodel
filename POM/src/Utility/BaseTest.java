package Utility;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;

public class BaseTest {
	
	public WebDriver driver;
	public ConfigDataProvider config;
	
	Logger log = LogManager.getLogger(BaseTest.class);
	 
	public WebDriver getDriver() {
		return driver;
	}
	
	@BeforeSuite
	public void SelectBrowser() {
		config = new ConfigDataProvider();
		log.info("Selecting the browser for the test");
		Browser.BrowserSelection(driver,config.BrowserName, config.APP_URL);
	}
	
	@AfterSuite
	public void CloseBrowser() {
		driver.quit();
	}

}
