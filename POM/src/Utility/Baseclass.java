package Utility;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;

public class Baseclass {
	
	Logger log = LogManager.getLogger(Baseclass.class);

	public WebDriver driver;
	//public ConfigDataProvider config;
	
	public WebDriver getDriver() {
        return driver;
    }

	public Baseclass(WebDriver driver) {
		PageFactory.initElements(driver, this);
	}
	
	public static void clickelement(WebElement element) {
		try {
			element.click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void sendkeys(WebElement element, String value) {
		try {
			element.sendKeys(value);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void waittime(int seconds) {
		try {
			Thread.sleep(seconds);
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	

}
